# Personal Server

Configs for my server with Debian 11 and Docker Compose. 

[TOC]

## Contain

- simple scripts for setting up a vps, creating a user, ssh, firewall, docker, restic, etc...
- a collection of docker-composite files for various services

## Server Setup

```bash
# Login to root in ssh
ssh -i ~/.ssh/<PATH_TO_KEY> root@<SERVER_IP>

# Install Git & Clone Repo
apt update -y
apt upgrade -y
apt install -y git
git clone https://gitlab.com/petcu/personal-server
cd personal-server/

# Edit variables in scripts
cd ./scripts
sed -i "s/USER=admin/USER=<YOUR_USER>/g" ./server.sh
sed -i "s/USER=admin/USER=<YOUR_USER>/g" ./docker.sh
sed -i "s/TZ=Europe\/Bucharest/TZ=<TIMEZONE>/g" ./server.sh
sed -i "s/PASS_ROOT=/PASS_ROOT=<YOUR_PASS>/g" ./server.sh
sed -i "s/PASS_ROOT=/PASS_ROOT=<PASS_USER>/g" ./server.sh

# Run script
chmod +x *.sh
./server.sh
./docker.sh
./restic.sh
```

## Inspired By

- https://github.com/awesome-selfhosted/awesome-selfhosted
- https://github.com/DoTheEvo/selfhosted-apps-docker
- https://github.com/BaptisteBdn/docker-selfhosted-apps


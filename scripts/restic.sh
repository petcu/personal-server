#!/bin/bash

# Install Dependencies
apt install -y curl jq wget

# Install Restic
REPO=https://api.github.com/repos/restic/restic/releases/latest
REPO_TEXT=$(curl ${REPO})

FILE=$(echo ${REPO_TEXT} | jq '.assets[].name' | grep linux_amd64 | sed -r 's/"//g' | sed -r 's/.bz2//g')
FILE_LINK=$(echo ${REPO_TEXT} | jq '.assets[].browser_download_url' | grep linux_amd64 | sed -r 's/"//g')

wget ${FILE_LINK}
bzip2 -dv ./${FILE}.bz2
chmod +x ./${FILE}

mv ${FILE} /usr/bin/restic
restic generate --bash-completion /etc/bash_completion.d/restic
source /etc/profile.d/bash_completion.sh

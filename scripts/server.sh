#!/bin/bash

# Variables
USER=admin
TZ=Europe/Bucharest
PASS_ROOT=
PASS_USER=

# Update & Upgrade
apt update
apt upgrade -y

# Change Timezone
sudo timedatectl set-timezone ${TZ}

# Disable Bash History
echo "HISTFILESIZE=0" >> ~/.bashrc
history -c; history -w
source ~/.bashrc

# Change Root Password
echo root:${PASS_ROOT} | chpasswd

# Create Admin User
useradd -m -s /bin/bash ${USER}
echo ${USER}:${PASS_USER} | chpasswd

# Copy SSH Key to Admin Home
mkdir -p /home/${USER}/.ssh
cp /root/.ssh/authorized_keys /home/${USER}/.ssh/authorized_keys
chown -R ${USER}:${USER} /home/${USER}/.ssh

# Disable Admin Bash History
sed -i -E 's/^HISTSIZE=/#HISTSIZE=/' /home/${USER}/.bashrc
sed -i -E 's/^HISTFILESIZE=/#HISTFILESIZE=/' /home/${USER}/.bashrc
echo "HISTFILESIZE=0" >> /home/${USER}/.bashrc

# Install and Set Sudo
apt install sudo -y
sed -i "s/admin/${USER}/g" ./config/sudoers
mv ./config/sudoers /etc/sudoers
chown -R root:root /etc/sudoers

# Config Auto-Updates
apt install -y unattended-upgrades
systemctl enable unattended-upgrades
systemctl start unattended-upgrades

cp ./config/50unattended-upgrades /etc/apt/apt.conf.d/
chown -R root:root /etc/apt/apt.conf.d/50unattended-upgrades

cp ./config/20auto-upgrades /etc/apt/apt.conf.d/
chown -R root:root /etc/apt/apt.conf.d/20auto-upgrades

# Disable Root Login and Password Auth
sed -i -E 's/^(#)?PermitRootLogin (prohibit-password|yes)/PermitRootLogin no/' /etc/ssh/sshd_config
sed -i -E 's/^(#)?PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
systemctl restart ssh

## Install UFW
apt install -y ufw

## Set Policy-es
ufw default allow outgoing
ufw default deny incoming
ufw allow ssh
ufw allow http/tcp

## Enable UFW
ufw enable

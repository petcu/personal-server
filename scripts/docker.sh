#!/bin/bash

# Variables
USER=admin

# Uninstall old versions
apt remove -y docker docker-engine docker.io containerd runc

# Install dependencies
apt install -y ca-certificates curl gnupg lsb-release

# Add Docker GPG Key
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Add Docker Repo
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker Engine
apt update -y
apt install -y docker-ce docker-ce-cli containerd.io

# Enable & Start Services
systemctl enable docker
systemctl start docker

# Add User to Docker Group
usermod -aG docker ${USER}

# Create a Private Network
docker network create proxy_net

# Download Compose Latest Binary
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose

# Set Permessions Compose
chmod +x /usr/local/bin/docker-compose
